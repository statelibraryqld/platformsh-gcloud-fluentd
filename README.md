# Platform.sh Google Cloud fluentd

A pre-configured fluentd for logging from a Platform.sh project to Google Cloud Logging

It forwards all of the logs from the standard Platform.sh container services, like Apache and nginx, but it ignores your app log. For that, use one of Google Cloud's Stackdriver packages within your app directly.

## Requirements

This environment variable needs to be set before running:

- GOOGLE_APPLICATION_CREDENTIALS - Path to JSON file containing authentication credentials

See the [Google Cloud authentication doc](https://github.com/googleapis/google-cloud-php/blob/master/AUTHENTICATION.md) for details

## Installing

The simplest way to install is to add the logging agent as a ruby dependency in `.platform.app.yml`:

```yaml
dependencies:
  ruby:
    platformsh_gcloud_fluentd: '~1.0'
```

Platform.sh will then install it automatically when building the image

Otherwise, if there is already have a `Gemfile` in use, add the package to that using `bundle add` and lock the version with `bundle lock` and it will then be installed along with the other packages

## Logging

Start the logger from the deploy hook in your app's `.platform.app.yml`:

```yaml
hooks:
  deploy: |
    bundle exec platformsh-gcloud-fluentd
```

From then on it should log everything correctly, assuming your credentials are set correctly
