Gem::Specification.new do |spec|
  spec.name        = 'platformsh_gcloud_fluentd'
  spec.version     = '0.1.0'
  spec.licenses    = ['ISC']
  spec.summary     = 'A pre-configured fluentd for logging from a Platform.sh project to Google Cloud Logging'
  spec.authors     = ['State Library of Queensland']
  spec.email       = 'webmaster@slq.qld.gov.au'
  spec.homepage    = 'https://gitlab.com/statelibraryqld/platformsh-gcloud-fluentd'
  spec.metadata    = {
    'source_code_uri' => 'https://gitlab.com/statelibraryqld/platformsh-gcloud-fluentd'
  }
  spec.bindir = 'bin'
  spec.executables = ['platformsh-gcloud-fluentd']
  spec.files = Dir[spec.bindir + '/*']
end
